Leonel Maia e Silva Junior - Teste de nível para Projetista de Sistemas Embarcados
==============================================================

### 1. Script monitor de recursos
Existe um arquivo bash, chamado script__monitor_recursos.sh .

Para a geração do executável, escreva o seguinte comando no terminal:

    chmod +x script_monitor_recursos.sh

Dentro do script existem algumas varáveis que servem como threshold para a geração do log, bem como a variável para o envio do email de alerta.

      fl_Using="0"       #Uso do fileSystem em %
      memory_Using="0"   #Uso da memória em %
      cpu_Using="0"      # Uso da CPU em %
      temp_treshold="0"  # Temperatura
      email="leonelmaiasj@gmail.com" #Email para envio do alerta

Apartir daí,quando executado e os limites não forem respeitados, o scrip deve-se criar um arquivo semelhante ao do exemplo a seguir.



>2020-02-05 17:35:44 - TIPO: FILESYSTEM - 32 % de Ulilizazao do Filesystem /dev/sda2
2020-02-05 17:35:45 - TIPO: MEMORIA    - 43 % da Memoria utilizada                                
2020-02-05 17:35:48 - TIPO: FILESYSTEM - 100 % de Ulilizazao do Filesystem /dev/loop7               
2020-02-05 17:35:49 - TIPO: CPU TEMP   - 54 graus é a Temperatura da CPU 1                               
2020-02-05 17:35:52 - TIPO: FILESYSTEM - 100 % de Ulilizazao do Filesystem /dev/loop9                    
2020-02-05 17:35:53 - TIPO: CPU TEMP   - 57 graus é a Temperatura da CPU 2

Neste momento o script ainda nao deve conseguir enviar alertas por email.Por isso, faz-se necessário a instalação da biblioteca mutt.

    sudo apt-get install mutt

Depois, com a biblioteca instalada, faz-se necessário a configuração do arquivo de usuário.(*.muttrc*)
    
    # Nome de quem vai enviar
    set realname="Leonel Maia"

    # Email de quem vai enviar
    set from="leonelmaiasj@gmail.com"

    # Usuario da conta de email
    set my_user="leonelmaiasj@gmail.com"

    # Senha da conta de email
    set my_pass='SENHA_DO_EMAIL'

    # Autenticacao no servidor smtp de email,nesse caso do gmail.com
    set smtp_url=smtps://$my_user:$my_pass@smtp.gmail.com

    # Camada de segurança, requerida pelo gmail.com
    set ssl_force_tls = yes 


Ademais, pode ocorrer de quando o script for executado e os limites forem criar um alerta, você receber uma mensagem do google avisando que não aceitou o acesso.Neste caso, será necessário mudar as configurações do GMAIL.

    Gerenciar contas -> segurança -> Acesso a app menos seguro e ativar.

Neste momento, o script ja deve enviar os alertas para os email.Porém, ainda não é executado pela crontab do sistema.Para isso, executamos o seguinte comando:

    crontab -e
Para acessarmos o arquivo e após a última linha comentada colocaremos o nosso comando para que o script execute periodicamente.Neste caso, ele foi adaptado para que seja executado rapidamente e facilite os testes.

    * * * * * sleep 10 && bash  /home/lesc/Desktop/Mobit/projetista-sis-embarc/questoes/Q1/script_monitor_recursos.sh

e fim da primeira questão.



### 2. Sincronizando o uso do log com várias threads
    
Nesta questão faltava algum método para sincronicar as threads que estavam sendo executadas, por isso que acontecia o problema de sincronismo.

De inicío, foi criado um arquivo para execução de comandos no diretório, o Makefile.Sendo assim, o make possui o comando de compilar e de fazer uma limpa dos binários.Por default, apenas o comando make ja executa o arquivo da questão 2.

Após a criação do arquivo, foi adicionado um mutex no código para o problema de sincronismo.Ademais, para imprimir os valores no <em>syslog</em>  foi usado a função system em conjunto com o comando logger.


Desta forma, para a compilação do arquivo da questão 2 e execução, basta executar o seguinte comando.
 
    make && ./q2

Neste repositório, o arquivo da questão 2(q2.cpp) ja envia o resultado para o syslog, podendo ser descomentada a parte que envia para o terminal.Por isso, para a verificação pode-se usar o seguinte comando


    tail -30 /var/log/syslog
Para visualizar apenas os comando enviados do programa da questão 2, ou o comando abaixo para uma visão mais geral do arquivo de syslog.

    nano  /var/log/syslog

Por fim, deve ser visível uma imagem parecida com o que vem a seguir.

>lesc@lesc-UFC:/var/log$ tail -30 /var/log/syslog
Feb  5 18:21:41 lesc-UFC lesc: Iniciando bloco 0
Feb  5 18:21:41 lesc-UFC lesc: Hello world from thread 0
Feb  5 18:21:41 lesc-UFC lesc: Fim do bloco 0
Feb  5 18:21:41 lesc-UFC lesc: Iniciando bloco 1
Feb  5 18:21:41 lesc-UFC lesc: Hello world from thread 1
Feb  5 18:21:41 lesc-UFC lesc: Fim do bloco 1
Feb  5 18:21:41 lesc-UFC lesc: Iniciando bloco 2
Feb  5 18:21:41 lesc-UFC lesc: Hello world from thread 2
Feb  5 18:21:41 lesc-UFC lesc: Fim do bloco 2
Feb  5 18:21:41 lesc-UFC lesc: Iniciando bloco 3
Feb  5 18:21:41 lesc-UFC lesc: Hello world from thread 3
Feb  5 18:21:41 lesc-UFC lesc: Fim do bloco 3
Feb  5 18:21:41 lesc-UFC lesc: Iniciando bloco 4
Feb  5 18:21:41 lesc-UFC lesc: Hello world from thread 4
Feb  5 18:21:41 lesc-UFC lesc: Fim do bloco 4
Feb  5 18:21:41 lesc-UFC lesc: Iniciando bloco 5
Feb  5 18:21:41 lesc-UFC lesc: Hello world from thread 5
Feb  5 18:21:41 lesc-UFC lesc: Fim do bloco 5
Feb  5 18:21:41 lesc-UFC lesc: Iniciando bloco 6
Feb  5 18:21:41 lesc-UFC lesc: Hello world from thread 6
Feb  5 18:21:41 lesc-UFC lesc: Fim do bloco 6
Feb  5 18:21:41 lesc-UFC lesc: Iniciando bloco 7
Feb  5 18:21:41 lesc-UFC lesc: Hello world from thread 7
Feb  5 18:21:41 lesc-UFC lesc: Fim do bloco 7
Feb  5 18:21:41 lesc-UFC lesc: Iniciando bloco 8
Feb  5 18:21:41 lesc-UFC lesc: Hello world from thread 8
Feb  5 18:21:41 lesc-UFC lesc: Fim do bloco 8
Feb  5 18:21:41 lesc-UFC lesc: Iniciando bloco 9
Feb  5 18:21:41 lesc-UFC lesc: Hello world from thread 9
Feb  5 18:21:41 lesc-UFC lesc: Fim do bloco 9

### 3. Produtor-Consumidor em C++
Na terceira questão foi inicialmente desenvolvido um arquivo de Makefile para a facilitação do processo de compilação.Apartir daí,  criou-se um arquivo Q3.cpp onde existem 2 threads, produtora e consumidora.

No desenvolvimento surgiram algumas dúvidas a respeito das threads e como elas deveriam se comportar.

1. Número de consumidores e produtores;  
2. Deveriam ser executados dentro de um laço?

Desta forma, deixei com 50 consumidores e um produtores, porém existe um DEFINE dentro do código chamado THREAD_COUNT, onde é possível definir o número de consumidores e produtores.À respeito da execução, foi considerada apenas 1 vez, entretanto existe um laço de <em>while(true)</em> comentado dentro de cada thread.

Para a execução do proposto na questão utiliza-se do mesmo princípio da anterior, onde executa-se o comando
         
    make && ./Q3
Sendo assim, você deve, após executar o comando anterior, ser capaz de visualizar o conteúdo de dentro do payload da mensagem.

### 4. Comunicação inter processos
Nesta questão, análogamente a questão anterior, foi desenvolvido um makefile logo de início e também apareceram algumas dúvidas pontuais.

1. Os processos estão rodando em máquinas diferentes ? ou iguais?

Desta forma, foi desenvolvido uma arquitetura em sockets e uma onde usa-se memória compartilhada. Ademais, no makefile existem 2 targets comentados, que são utilizados para a escolha da metodologia, via socket ou via memória compartilhada. 

    #TARGET=server     #Deve ser comentado quando for usada memória compartilhada
    #TARGET2=client    #Deve ser comentado quando for usada memória compartilhada
    TARGET=shm_write   #Deve ser comentado quando for usado socket
    TARGET2=shm_read   #Deve ser comentado quando for usado socket

Sendo assim, foi simulado uma webcam por uma imagem, onde são realizadas 5 leituras da mesma e ela é colocada na memória compartilhada.E, ao mesmo tempo, temos uma simulação semelhante utilizando sokets.

Ainda, no caso da memória compartilhada, a imagem foi passada para um arquivo txt com sua tradução devido ao tempo.Além disso,  o tamanho é passada na struct.

Acredito que existem muitas melhorias a serem feitas nos processos desta questão que não foram desenvolvidos, como por exemplo um uso mais abrangente de sinais, que serviriam para evitar grandes esperas.

Por fim, a execução desta questão é levemente diferente das anteriores pois utiliza dois terminais.Executa-se em um deles um comando bem semelhante as questoes anteriores.
     
     make && ./shm_write
e no outro apenas a execução do outro processo.

    ./shm_read
Vale ressaltar que os comandos mudam os executaveis dependendo da metodologia desejada. 



