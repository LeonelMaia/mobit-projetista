/**
  ******************************************************************************
  * @file    Q3.cpp
  * @author  Leonel Maia
  * @brief   Questão 3 do teste para projetista de sistemas embarcados
  *          da mobit 
   @verbatim
  ==============================================================================
                    ##### Pr�-Requisitos #####
  ==============================================================================
  [..]
    Nenhum


            ##### Como utilizar este codigo #####
  ==============================================================================
  [..]
   (#) Execute o makefile.

   (#) Execute o codigo no terminal do linux

  @endverbatim
  */


#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <fstream>
#include <string>
#include <queue>
#include <unistd.h>

using namespace std;

/*Definicao dos comandos e seus respectivos tamanhos*/
#define START_SEQ             "<payload>"
#define END_SEQ               "</payload>"
#define START_SEQ_SIZE        9
#define END_SEQ_SIZE          (START_SEQ_SIZE + 1)
/*Definicao do número de threads simultaneas*/
#define THREAD_COUNT          50
/*Definicao do tamanho maximo da fila*/
#define QUEUE_SIZE_MAX        1000

/*Declaracao do Mutex e da fila*/
std::mutex mu;
queue < string > fila;

/**
  * @brief  Funçao produtora responsável por escrever na fila
  * @note   Escreve na fila compartilhada.
  * @param  id:  Identificador das threads
  * @retval void
  */
static void produtora(int id) {
   /*Declaracao da linha e da variavel de tamanho da fila*/
    string line;
    int size_fila;
  //  while (1) {
    /*Armazenamento do tamanho atual da fila via mutex*/ 
        mu.lock();
        size_fila = fila.size();
        mu.unlock();
        /*Verificaçao sobre o tamanho máximo da fila*/
        if (size_fila > QUEUE_SIZE_MAX) {
            usleep(5000);
    //        continue;
        }
        /*Leitura do arquivo*/
        ifstream myfile("input.xml");
        if (myfile.is_open()) {
            while (!myfile.eof()) {
                getline(myfile, line);
                //cout << line << endl;
                /*Acesso a fila*/
                mu.lock();
                fila.push(line);
                mu.unlock();
            }
            myfile.close();
        }
   // }
}


/**
  * @brief  Funçao consumidota responsável por ler da fila
  *         e printar no terminal caso seja o payload
  * @note   Escreve na fila compartilhada.
  * @param  id:  Identificador das threads
  * @retval void
  */
static void consumidora(int id) {
    /*Declaracao da linha e das variaveis da fila*/
    string line, substr, payload;
    bool fila_vazia;
    int size_fila;
    //while (true) {
    /*Armazenamento do tamanho atual da fila via mutex*/ 
        mu.lock();
        fila_vazia = fila.empty();
        size_fila = fila.size();
        mu.unlock();
        /*Verificaçao sobre o tamanho minimo da fila*/
        while (!fila_vazia && size_fila > 20) {
            /*Acessando o elemento do topo da fila FIFO*/
            mu.lock();
            line = fila.front();
            mu.unlock();
            /*Tratamento e tomada de decisão a respeito de cada linha da fila*/
            if (line.size() > 2) substr = line.substr(2, START_SEQ_SIZE); // 2 é o Numero de espaços em branco nas linhas de interesse
            if (substr == START_SEQ) {
                substr = line.substr(line.size() - END_SEQ_SIZE, END_SEQ_SIZE);
                if (substr == END_SEQ) {
                    payload = line.substr((START_SEQ_SIZE + 2), line.size() - END_SEQ_SIZE - START_SEQ_SIZE - 2);
                    cout << payload << endl;
                    /*Remocao do elemento analisado da fila*/
                    mu.lock();
                    fila.pop();
                    mu.unlock();
                    break;
                }
            }
            mu.lock();
            fila.pop();
            mu.unlock();
        }
    //}
}

int main() {
    /* Inicializaçao do vetor de Threads*/
    std::vector < std::thread > v;

    /*Lançamento das threads produtoras e consumidoras*/
    for (size_t i = 0; i < THREAD_COUNT; i++) {
        v.emplace_back(produtora, i);
    }
    for (size_t i = 0; i < THREAD_COUNT; i++) {
        v.emplace_back(consumidora, i);
    }
    for (auto & t: v) {
        t.join();
    }
    return 0;
}