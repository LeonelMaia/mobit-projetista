#---------------------------------------------------------------
#                     MONITORAMENTO DE RECURSOS (Questão 1)
#---------------------------------------------------------------

fl_Using="0"
memory_Using="0"
cpu_Using="0"
temp_treshold="0"
email="leonelmaiasj@gmail.com"

#Analise do sistema de arquivos
ve_filesystem ()
{
df -h | grep -v ^Filesystem | while read line
do
percent=`echo $line | awk '{ print $5 }' | sed 's/%//g'`
if [ "$percent" -gt $fl_Using ]; then 
echo "`date +"%Y-%m-%d %H:%M:%S"` - TIPO: FILESYSTEM - $percent % de Ulilizazao do Filesystem `echo $line | awk '{ print $1 

}'`" >> /home/lesc/Desktop/Mobit/projetista-sis-embarc/questoes/Q1/centraldealertas.txt
echo "`date +"%Y-%m-%d %H:%M:%S"` - TIPO: FILESYSTEM - $percent % de Ulilizazao do Filesystem `echo $line | awk '{ print $1 

}'`" | mutt -s 'assunto do e-mail' $email
fi
done
}

#Analise da memoria
ve_memoria ()
{
percent=`free -m | awk '/^Mem/{ print $3,"*100","/",$2}' | sed 's/ //g' | bc`
if [ "$percent" -gt $memory_Using ]; then 
echo "`date +"%Y-%m-%d %H:%M:%S"` - TIPO: MEMORIA    - $percent % da Memoria utilizada" >> /home/lesc/Desktop/Mobit/projetista-sis-embarc/questoes/Q1/centraldealertas.txt
echo "`date +"%Y-%m-%d %H:%M:%S"` - TIPO: MEMORIA    - $percent % da Memoria utilizada"| mutt -s 'assunto do e-mail' $email
fi
}

#Analise da temperatura da CPU
ve_cpu_temp ()
{
ncpus="0"
cat /sys/class/thermal/thermal_zone*/temp | while read line
do
percent=$line
((ncpus=ncpus+1))
if [ "$percent" -gt $temp_treshold ]; then 
echo "`date +"%Y-%m-%d %H:%M:%S"` - TIPO: CPU TEMP   - `(expr $percent / 1000)` graus é a Temperatura da CPU $ncpus" >>  /home/lesc/Desktop/Mobit/projetista-sis-embarc/questoes/Q1/centraldealertas.txt

echo "`date +"%Y-%m-%d %H:%M:%S"` - TIPO: CPU TEMP   - `(expr $percent / 1000)` graus é a Temperatura da CPU $ncpus"| mutt -s 'assunto do e-mail' $email
fi
done
}

#Analise da CPU
ve_cpu ()
{
temp=`vmstat 1 2 | sed 1,3d | awk '{ print $('$locale') }'`
if [ "$percent" -gt $cpu_Using ]; then 
echo "`date +"%Y-%m-%d %H:%M:%S"` - TIPO: CPU LOAD   - `expr 100 - $percent` % de CPU Utilizada" >>  /home/lesc/Desktop/Mobit/projetista-sis-embarc/questoes/Q1 centraldealertas.txt
echo "`date +"%Y-%m-%d %H:%M:%S"` - TIPO: CPU LOAD   - `expr 100 - $percent` % de CPU Utilizada"| mutt -s 'assunto do e-mail' $email
fi
}


start ()
{
ve_filesystem
ve_memoria
ve_cpu
ve_cpu_temp 
}

start 
