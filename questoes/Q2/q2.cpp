/**
  ******************************************************************************
  * @file    q2.cpp
  * @author  Leonel Maia
  * @brief   Questão 2 do teste para projetista de sistemas embarcados
  *          da mobit 
   @verbatim
  ==============================================================================
                    ##### Pr�-Requisitos #####
  ==============================================================================
  [..]
    Nenhum


            ##### Como utilizar este codigo #####
  ==============================================================================
  [..]
   (#) Execute o makefile.

   (#) Execute o codigo no terminal do linux

  @endverbatim
  */


#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <string>

/*Definicao de comandos que serão usados */
#define START_COMMAND     "logger Iniciando bloco %d"
#define HELLO_COMMAND     "logger Hello world from thread %d"
#define END_COMMAND       "logger Fim do bloco %d"

/*Declaração do número de Threads*/
static const int THREAD_COUNT = 10;
/*Declaração do Mutex*/
std::mutex mu;
/*Declaração da string de comandos*/
char command[60];

/**
  * @brief  Printa o log do processo
  * @note   Funcao principal da questão.
  * @param  id:  Identificador das threads
  * @retval void
  */
static void print_log(int id)
{
    mu.lock();
   
   /* std::cout << "--------------------" <<'\n';
    std::cout << "Iniciando bloco " << id << '\n';
    std::cout << "Hello world from thread " << id << '\n';
    std::cout << "Fim do bloco " << id << '\n';
    std::cout << "--------------------" << '\n';
    */
    snprintf(command, 60, START_COMMAND, id);
    system(command);
    snprintf(command, 60, HELLO_COMMAND, id);
    system(command);
    snprintf(command, 60, END_COMMAND, id);
    system(command);	
    
    mu.unlock();
}

int main()
{
    /* Inicializaçao do vetor de Threads*/
    std::vector<std::thread> v;

    for (size_t i = 0; i < THREAD_COUNT; i++)
    {  
        v.emplace_back(print_log,i);
    }
    for (auto& t : v)
    {
        t.join();
    }
    return 0; 
}
