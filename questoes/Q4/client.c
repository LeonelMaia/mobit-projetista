/**
  ******************************************************************************
  * @file    shm_read.c
  * @author  Leonel Maia
  * @brief   Questão 4 do teste para projetista de sistemas embarcados
  *          da mobit 
   @verbatim
  ==============================================================================
                    ##### Pr�-Requisitos #####
  ==============================================================================
  [..]
    Nenhum


            ##### Como utilizar este codigo #####
  ==============================================================================
  [..]
   (#) Execute o makefile.

   (#) Execute o codigo no terminal do linux

  @endverbatim
  */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <png.h>

/*Prototipos das funcoes*/
void read_image(png_bytepp * image, int * nbytes);
char * itoa(int val, int base);
void print_image(png_bytepp image) ;


int main(int argc, char** argv){
/*Declaracao das variaveis de arquivos , Socket e suas diretivas*/

    png_bytepp image;
    int nbytes;
    int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    int len;
    struct addrinfo info, * result;
    memset( & info, 0, sizeof(struct addrinfo));
    info.ai_family = AF_INET;
    info.ai_socktype = SOCK_STREAM;

  /* Traducao do ip e porta para a struct */
    if (0 != getaddrinfo("localhost", "8080", & info, & result))
        exit(1);

    /* Conectando o socket com o server */
    connect(sock_fd, result->ai_addr, result->ai_addrlen);
    /*Lendo a imagem e seu tamanho*/
    read_image(&image,&nbytes);
    char * buffer = "Ola Server!!\r\n\r\n";
    buffer = itoa(nbytes, 10);
    write(sock_fd, buffer, strlen(buffer));
    printf("Enviando: %s \n", buffer);
    printf("Server! é o Leonel\n");
    printf("Tamanho da imagem e: %d \n", nbytes);
    for (int j = 0; j < 48; j++) {
        len = write(sock_fd, image[j], 50);
        if (len < 0) {
            error("ERROR");
        }
    }
    char resp[1000];
    len = read(sock_fd, resp, 999);
    resp[len] = '\0';
    printf("%s\n", resp);
    print_image(image);
    return 0;
}


/**
  * @brief Funcao para conversao de inteiros e strings
  * @note   Funcao usada para testes
  * @param  val:  valor 
  * @param  base:  base (binaria,decimal...)
  * @retval buffer
  */
char * itoa(int val, int base) {

    static char buf[32] = {
        0
    };

    int i = 30;

    for (; val && i; --i, val /= base)

        buf[i] = "0123456789abcdef" [val % base];

    return &buf[i + 1];

}

/**
  * @brief  Ler imagem png
  * @note   Funcao usada para testes
  * @param  image:  ponteiro para a imagem da libpng
  * @param  nbytes:  ponteiro inteiro que representa o tamanho da imagem
  * @retval void
  */
void read_image(png_bytepp * image, int * nbytes) {
    const char * png_file = "tantei-san.png";
    png_structp png_ptr;
    png_infop info_ptr;
    FILE * fp;
    png_uint_32 width;
    png_uint_32 height;
    int bit_depth;
    int color_type;
    int interlace_method;
    int compression_method;
    int filter_method;
    int j;
    png_bytepp rows;
    fp = fopen(png_file, "rb");
    if (!fp) {
        printf("Erro a abrir o arquivo\n");
    }
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) {
        printf("Erro no PNG");
    }
    info_ptr = png_create_info_struct(png_ptr);
    if (!png_ptr) {
        printf("Error ao criar a estrutura png");
    }
    png_init_io(png_ptr, fp);
    png_read_png(png_ptr, info_ptr, 0, 0);
    png_get_IHDR(png_ptr, info_ptr, & width, & height, & bit_depth, &
        color_type, & interlace_method, & compression_method, &
        filter_method);
    * image = png_get_rows(png_ptr, info_ptr);
    printf("Width is %d, height is %d\n", width, height);
    int rowbytes;
    rowbytes = png_get_rowbytes(png_ptr, info_ptr);
    printf("Row bytes = %d\n", rowbytes);
    * nbytes = rowbytes * height;

}

/**
  * @brief  Printa a imagem no terminal de forma
  *         alternativa
  * @note   Funcao usada para testes
  * @param  image:  ponteiro para a imagem da libpng
  * @retval void
  */
void print_image(png_bytepp image) {

    for (int j = 0; j < 48; j++) {
        int i;
        png_bytep row;
        row = image[j];
        for (i = 0; i < 50; i++) {
            png_byte pixel;
            pixel = row[i];
            if (pixel < 64) {
                printf("##");
            } else if (pixel < 128) {
                printf("**");
            } else if (pixel < 196) {
                printf("..");
            } else {
                printf("  ");
            }

        }
        printf("\n");
    }
}