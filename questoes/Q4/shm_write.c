/**
  ******************************************************************************
  * @file    shm_write.c
  * @author  Leonel Maia
  * @brief   Questão 4 do teste para projetista de sistemas embarcados
  *          da mobit 
   @verbatim
  ==============================================================================
                    ##### Pr�-Requisitos #####
  ==============================================================================
  [..]
    Nenhum


            ##### Como utilizar este codigo #####
  ==============================================================================
  [..]
   (#) Execute o makefile.

   (#) Execute o codigo no terminal do linux

  @endverbatim
  */

#include<stdio.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<sys/types.h>
#include<string.h>
#include<errno.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include <png.h>

/*Definiçoes a respeito da memoria compartilhada*/
#define BUF_SIZE 1024
#define SHM_KEY 0x1234

/**
  * @brief Estrutura principal da memoria compartilhada
  */
struct shmseg {
    int cnt;
    int complete;
    char buf[BUF_SIZE];
};

/*Prototipos das funcoes*/
void read_image(png_bytepp * image, int * nbytes);
int fill_buffer(char * bufptr, int size);
void print_image(png_bytepp image);
void print_image_teste(int * buff);


int main(int argc, char * argv[]) {
    /*Declaracao das variaveis de imagem e memoria compartilhada*/
    png_bytepp * image_addr;
    int nbytes;
    int shmid, numtimes;
    struct shmseg * shmp;
    char * bufptr;
    int spaceavailable;
    int buf_image[4800];
    /*Criaçao do espaço de memoria compartilhada*/
    shmid = shmget(SHM_KEY, sizeof(struct shmseg), 0666 | IPC_CREAT);
    if (shmid == -1) {
        perror("Shared memory");
        return 1;
    }

     /* Solicitando segmento de memoria.*/
    shmp = shmat(shmid, NULL, 0);
    if (shmp == (void * ) - 1) {
        perror("Shared memory attach");
        return 1;
    }
    /* Passando a imagem para o buffer*/
    bufptr = shmp->buf;
    image_addr = & buf_image;
    read_image( & image_addr, & nbytes);
    for (int j = 0; j < 48; j++) {
        int i;
        png_bytep row;
        row = image_addr[j];
        for (i = 0; i < 50; i++) {
            png_byte pixel;
            pixel = row[i];
            bufptr[(50 * j) + i] = pixel;
        }
    }
    /*Teste de manipulacao dos dados*/
    print_image_teste(bufptr);
    spaceavailable = BUF_SIZE;
    /* Transferencia do bloco de dados para saida*/
    for (numtimes = 0; numtimes < 5; numtimes++) {
        shmp->cnt = 2400;
        shmp->complete = 0;
        printf("Writing Process: Shared Memory Write: Wrote %d bytes\n", shmp->cnt);
        bufptr = shmp->buf;
        spaceavailable = BUF_SIZE;
        sleep(3);
    }
    printf("Writing Process: Wrote %d times\n", numtimes);
    shmp->complete = 1;

    if (shmdt(shmp) == -1) {
        perror("shmdt");
        return 1;
    }

    if (shmctl(shmid, IPC_RMID, 0) == -1) {
        perror("shmctl");
        return 1;
    }
    printf("Writing Process: Complete\n");
    return 0;
}

/**
  * @brief  Ler imagem png
  * @note   Funcao usada para testes
  * @param  image:  ponteiro para a imagem da libpng
  * @param  nbytes:  ponteiro inteiro que representa o tamanho da imagem
  * @retval void
  */
void read_image(png_bytepp * image, int * nbytes) {
    const char * png_file = "tantei-san.png";
    png_structp png_ptr;
    png_infop info_ptr;
    FILE * fp;
    png_uint_32 width;
    png_uint_32 height;
    int bit_depth;
    int color_type;
    int interlace_method;
    int compression_method;
    int filter_method;
    int j;
    png_bytepp rows;
    fp = fopen(png_file, "rb");
    if (!fp) {
        printf("Cannot open the file\n");
    }
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) {
        printf("Cannot create PNG read structure");
    }
    info_ptr = png_create_info_struct(png_ptr);
    if (!png_ptr) {
        printf("Cannot create PNG info structure");
    }
    png_init_io(png_ptr, fp);
    png_read_png(png_ptr, info_ptr, 0, 0);
    png_get_IHDR(png_ptr, info_ptr, & width, & height, & bit_depth, &
        color_type, & interlace_method, & compression_method, &
        filter_method);
    * image = png_get_rows(png_ptr, info_ptr);
    printf("Width is %d, height is %d\n", width, height);
    int rowbytes;
    rowbytes = png_get_rowbytes(png_ptr, info_ptr);
    printf("Row bytes = %d\n", rowbytes);
    * nbytes = rowbytes * height;

}
/**
  * @brief  Printa a imagem no terminal de forma
  *         alternativa
  * @note   Funcao usada para testes
  * @param  image:  ponteiro para a imagem da libpng
  * @retval void
  */
void print_image(png_bytepp image) {

    for (int j = 0; j < 48; j++) {
        int i;
        png_bytep row;
        row = image[j];
        for (i = 0; i < 50; i++) {
            png_byte pixel;
            pixel = row[i];
            if (pixel < 64) {
                printf("##");
            } else if (pixel < 128) {
                printf("**");
            } else if (pixel < 196) {
                printf("..");
            } else {
                printf("  ");
            }

        }
        printf("\n");
    }
}

/**
  * @brief  Printa a imagem no terminal de forma
  *         alternativa para verificaçao no buffer
  * @note   Funcao usada para testes
  * @param  buffer_image:  ponteiro para a imagem no buffer
  * @retval void
  */
void print_image_teste(int * buffer_image) {

    for (int j = 0; j < 48; j++) {
        int i;
        png_bytep row;
        row = buffer_image;
        for (i = 0; i < 50; i++) {
            png_byte pixel;
            pixel = row[(j * 50) + i];
            if (pixel < 64) {
                printf("##");
            } else if (pixel < 128) {
                printf("**");
            } else if (pixel < 196) {
                printf("..");
            } else {
                printf("  ");
            }

        }
        printf("\n");
    }

    return 0;
}