/**
  ******************************************************************************
  * @file    shm_read.c
  * @author  Leonel Maia
  * @brief   Questão 4 do teste para projetista de sistemas embarcados
  *          da mobit 
   @verbatim
  ==============================================================================
                    ##### Pr�-Requisitos #####
  ==============================================================================
  [..]
    Nenhum


            ##### Como utilizar este codigo #####
  ==============================================================================
  [..]
   (#) Execute o makefile.

   (#) Execute o codigo no terminal do linux

  @endverbatim
  */
#include<stdio.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<sys/types.h>
#include<string.h>
#include<errno.h>
#include<stdlib.h>
#include <png.h>
/*Definiçoes a respeito da memoria compartilhada*/
#define BUF_SIZE 1024
#define SHM_KEY 0x1234


/**
  * @brief Estrutura principal da memoria compartilhada
  */
struct shmseg {
    int cnt;
    int complete;
    char buf[BUF_SIZE];
};

/*Prototipos das funcoes*/
void print_image_teste(int * buffer_image);
void escrever_arquivo(FILE *pont_arq , char* palavra);


int main(int argc, char * argv[]) {
    /*Declaracao das variaveis de arquivos e memoria compartilhada*/
    FILE *pont_arq;
    int shmid;
    struct shmseg * shmp;

    /*Criaçao do espaço de memoria compartilhada*/
    shmid = shmget(SHM_KEY, sizeof(struct shmseg), 0644 | IPC_CREAT);
    if (shmid == -1) {
        perror("Shared memory");
        return 1;
    }

    /* Solicitando segmento de memoria.*/
    shmp = shmat(shmid, NULL, 0);
    if (shmp == (void * ) - 1) {
        perror("Shared memory attach");
        return 1;
    }

    /* Transferencia do bloco de dados para saida*/
    while (shmp->complete != 1) {

        printf("Memory contains :\n\n");
        if (shmp->cnt > 1) {
            print_image_teste(shmp->buf);
            escrever_arquivo(pont_arq , shmp->buf);
        }

        if (shmp->cnt == -1) {
            perror("read");
            return 1;
        }
        printf("Reading Process: Shared Memory: Read %d bytes\n", shmp->cnt);
        sleep(3);
    }
    /*Processo de transferencia concluido* e a memoria vai ser liberada*/
    printf("Reading Process: Reading Done, Detaching Shared Memory\n");
    if (shmdt(shmp) == -1) {
        perror("shmdt");
        return 1;
    }
    printf("Reading Process: Complete\n");
    return 0;
}


/**
  * @brief  Printa a imagem no terminal de forma
  *         alternativa
  * @note   Funcao usada para testes
  * @param  buffer_image0:  ponteiro para a imagem
  * @retval void
  */

void print_image_teste(int * buffer_image) {

    for (int j = 0; j < 48; j++) {
        int i;
        png_bytep row;
        row = buffer_image;
        for (i = 0; i < 50; i++) {
            png_byte pixel;
            pixel = row[(j * 50) + i];
            if (pixel < 64) {
                printf("##");
            } else if (pixel < 128) {
                printf("**");
            } else if (pixel < 196) {
                printf("..");
            } else {
                printf("  ");
            }

        }
        printf("\n");
    }

}

/**
  * @brief Funcao para a escrita da imagem no arquivo
  * @note   Funcao usada para testes e necessaria ao desafio
  * @param  pont_arq:  ponteiro para arquivo
  * @param  palavra:   ponteiro para o que vai ser escrito
  * @retval void
  */
void escrever_arquivo(FILE *pont_arq , char* palavra)
{ 
  /*Abrindo o arquivo*/
  pont_arq = fopen("arquivo_imagem_Shared_Memory.txt", "w"); 
  /*Teste para ver arquivo foi realmente criado*/
  if(pont_arq == NULL)
  {
  printf("Erro na abertura do arquivo!");
  }
  /*Escrita no arquivo*/
  fprintf(pont_arq, "%s \n \n", palavra);
  fprintf(pont_arq,"\n \n");
  fprintf(pont_arq,"USANDO A 'TRADUÇÃO DA IMAGEM' \n \n");
  fprintf(pont_arq,"\n \n");
  for (int j = 0; j < 48; j++) {
    int i;
    png_bytep row;
    row = palavra;
    for (i = 0; i < 50; i++) {
        png_byte pixel;
        pixel = row[(j * 50) + i];
        if (pixel < 64) {
            fprintf(pont_arq,"##");
        } else if (pixel < 128) {
            fprintf(pont_arq,"**");
        } else if (pixel < 196) {
            fprintf(pont_arq,"..");
        } else {
            fprintf(pont_arq,"  ");
        }

    }
        fprintf(pont_arq,"\n");
    }
    /*Fechando o arquivo */
  fclose(pont_arq);
  printf("Dados gravados com sucesso!");
}