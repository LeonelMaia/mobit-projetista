/**
  ******************************************************************************
  * @file    shm_read.c
  * @author  Leonel Maia
  * @brief   Questão 4 do teste para projetista de sistemas embarcados
  *          da mobit 
   @verbatim
  ==============================================================================
                    ##### Pr�-Requisitos #####
  ==============================================================================
  [..]
    Nenhum


            ##### Como utilizar este codigo #####
  ==============================================================================
  [..]
   (#) Execute o makefile.

   (#) Execute o codigo no terminal do linux

  @endverbatim
  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <png.h>


/*Prototipos das funcoes*/
void print_image_teste(int * buffer_image);
void escrever_arquivo(FILE *pont_arq , char* palavra);

int main(int argc, char** argv){
/*Declaracao das variaveis de arquivos , Socket e suas diretivas*/
  FILE *pont_arq;
  int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
  struct addrinfo directives, *result;
  memset(&directives, 0, sizeof(struct addrinfo));
  directives.ai_family = AF_INET;
  directives.ai_socktype = SOCK_SEQPACKET;
  directives.ai_flags = AI_PASSIVE;

  /* Traducao do ip e porta para a struct */
  if(0 !=  getaddrinfo(NULL, "8080", &directives, &result))
    exit(1);
 
  /* Ligacao porta -> socket */
  if(bind(sock_fd, result->ai_addr, result->ai_addrlen) != 0)
      exit(1);
  /* Socket vai entrar no modo "Escuta"*/
  if(listen(sock_fd, 10) != 0)
      exit(1);

  printf("Esperando conexao no http://localhost:8080 ...\n");
  
  while(1){
   
    /* Conexao Aceita */
    int client_fd = accept(sock_fd, NULL, NULL); 
    char buffer[10];
    int len = read(client_fd, buffer, 4);
    int buffer_image [atol(buffer)];
    buffer[len] = '\0';
    /*Envio de mensagem ao cliente*/
    char * connected_message = "VOCE ESTA CONECTADO!!";
    write(client_fd, connected_message, strlen(connected_message)); 
    /*Recepcao de mensagem*/
    printf("=== Cliente enviou ===\n");
    printf("%s\n", buffer);
    len = 0;
    sleep(3);
    len = read(client_fd, buffer_image, atol(buffer));
     if (len < 0){
      printf("ERROR para ler o socket");
      exit(1);
     } 
    

    buffer_image[len] = '\0';
    int count = 0;
    for (int del = 0 ; del < 100000 ; del ++){
    count++;
   }
    printf("LER IMAGEM");
    printf("\nLemos %d pixels\n", len);
    printf("=== Client Enviou ===\n");
  
    print_image_teste(buffer_image);
    escrever_arquivo(pont_arq , buffer_image);



    close(client_fd);
    sleep(1);
  }

  return 0;
}

/**
  * @brief  Printa a imagem no terminal de forma
  *         alternativa
  * @note   Funcao usada para testes
  * @param  buffer_image0:  ponteiro para a imagem
  * @retval void
  */

void print_image_teste(int * buffer_image) {

    for (int j = 0; j < 48; j++) {
        int i;
        png_bytep row;
        row = buffer_image;
        for (i = 0; i < 50; i++) {
            png_byte pixel;
            pixel = row[(j * 50) + i];
            if (pixel < 64) {
                printf("##");
            } else if (pixel < 128) {
                printf("**");
            } else if (pixel < 196) {
                printf("..");
            } else {
                printf("  ");
            }

        }
        printf("\n");
    }

}

/**
  * @brief Funcao para a escrita da imagem no arquivo
  * @note   Funcao usada para testes e necessaria ao desafio
  * @param  pont_arq:  ponteiro para arquivo
  * @param  palavra:   ponteiro para o que vai ser escrito
  * @retval void
  */
void escrever_arquivo(FILE *pont_arq , char* palavra)
{ 
  /*Abrindo o arquivo*/
  pont_arq = fopen("arquivo_imagem_SOCKET.txt", "w"); 
  /*Teste para ver arquivo foi realmente criado*/
  if(pont_arq == NULL)
  {
  printf("Erro na abertura do arquivo!");
  }
  /*Escrita no arquivo*/
  fprintf(pont_arq, "%s \n \n", palavra);
  fprintf(pont_arq,"\n \n");
  fprintf(pont_arq,"USANDO A 'TRADUÇÃO DA IMAGEM' \n \n");
  fprintf(pont_arq,"\n \n");
  for (int j = 0; j < 48; j++) {
    int i;
    png_bytep row;
    row = palavra;
    for (i = 0; i < 50; i++) {
        png_byte pixel;
        pixel = row[(j * 50) + i];
        if (pixel < 64) {
            fprintf(pont_arq,"##");
        } else if (pixel < 128) {
            fprintf(pont_arq,"**");
        } else if (pixel < 196) {
            fprintf(pont_arq,"..");
        } else {
            fprintf(pont_arq,"  ");
        }

    }
        fprintf(pont_arq,"\n");
    }
    /*Fechando o arquivo */
  fclose(pont_arq);
  printf("Dados gravados com sucesso!");
}